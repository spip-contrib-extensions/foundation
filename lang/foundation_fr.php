<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'desactiver' => 'Désactiver Foundation',
	'foundation_titre' => 'Foundation',
	'titre_page_configurer_foundation' => 'Configuration de Fondation',
	'foundation_3' => 'Foundation 3',
	'foundation_4' => 'Foundation 4',
	'foundation_5' => 'Foundation 5',
	'activer_javascript' => 'Activer les javascripts de foundation ?',
	'activer_javascript_explication' => "Si vous n'utilisez pas les composants javascripts de foundation il est inutile de les charger dans SPIP.",
	'fichier_htc' => 'Activer le fichier boxsizing.htc ?',
	'fichier_htc_explication' => '<strong>Exprimental:</strong> Ce fichier peu améliorer la gestion de foundation sous IE7. Il faudra activer les fichier .htc dans le fichier .htaccess en ajoutant "AddType text/x-component .htc".',
	'version' => 'Version de foundation',
	'version_explication' => "Choisir la version de foundation à utiliser dans l'espace public.",
	'icons' => 'Utiliser les icones Foundation ?',
	'icons_explication' => 'Ajouter les CSS pour les <a href="http://zurb.com/playground/foundation-icon-fonts-3" title="foundation icon">icones foundation</a>.',
	'syntaxe_deprecie' => 'La syntaxe #ARRAY{nombre, type} est dépréciée au profit de #ARRAY{type, nombre} et sera supprimée dans les futurs versions de foundation',
	'erreur_js' => "L'utilisation du squelette @squelette@ requière l'activation du javascript de foundation"
);